import { Observable } from 'rxjs';
import { Task } from '../models/task.entity';
import { TaskStatus } from '../models/task-status.model';

export interface ITaskRepository {
  createTask(title: string, description: string): Observable<Task>;
  getTasks(): Observable<Task[]>;
  getTaskById(id: string): Observable<Task | null>;
  getTasksByStatus(status: TaskStatus): Observable<Task[]>;
  updateTask(task: Task): Observable<Task>;
  deleteTask(id: string): Observable<Task>;
}
