import { NestFactory } from '@nestjs/core';
import { PresentersModule } from './presenters/presenters.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(PresentersModule);

  const config = new DocumentBuilder()
    .setTitle('Clean Architecture Todo API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(3000);
}
bootstrap();
