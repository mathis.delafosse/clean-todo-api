import { Module } from '@nestjs/common';
import { TASK_REPOSITORY_TOKEN } from '../presenters/inject-tokens';
import { JsonTaskRepository } from './json-task.repository';
import * as process from 'process';
import { InMemoryTaskRepository } from './in-memory-task.repository';

@Module({
  providers: [
    {
      provide: TASK_REPOSITORY_TOKEN,
      useClass:
        process.env.data_provider === 'json'
          ? JsonTaskRepository
          : InMemoryTaskRepository,
    },
  ],
  exports: [TASK_REPOSITORY_TOKEN],
})
export class InfraModule {}
