import { ITaskRepository } from '../domain/adapters/task-repository';
import { Observable, of } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { Task } from '../domain/models/task.entity';
import { TaskStatus } from '../domain/models/task-status.model';

@Injectable()
export class InMemoryTaskRepository implements ITaskRepository {
  private tasks = new Map<string, any>();

  createTask(title: string, description: string): Observable<Task> {
    const task = new Task(
      this.tasks.size.toString(),
      title,
      description,
      TaskStatus.OPEN,
    );

    this.tasks.set(task.id, task);

    return of(task);
  }

  getTasks() {
    return of(Array.from(this.tasks.values()));
  }

  getTaskById(id: string): Observable<Task | null> {
    return of(this.tasks.get(id));
  }

  getTasksByStatus(status: TaskStatus): Observable<Task[]> {
    return of(
      Array.from(this.tasks.values()).filter((task) => task.status === status),
    );
  }

  updateTask(task: Task): Observable<Task> {
    this.tasks.set(task.id, task);

    return of(task);
  }

  deleteTask(id: string): Observable<Task> {
    const task = this.tasks.get(id);

    this.tasks.delete(id);

    return of(task);
  }
}
