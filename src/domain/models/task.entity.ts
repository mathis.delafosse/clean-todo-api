import { ApiProperty } from '@nestjs/swagger';
import { TaskStatus } from './task-status.model';

export class Task {
  @ApiProperty()
  id: string;

  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string;

  @ApiProperty({ enum: TaskStatus, example: TaskStatus.IN_PROGRESS })
  status: TaskStatus;

  constructor(
    id: string,
    title: string,
    description: string,
    status: TaskStatus,
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.status = status;
  }
}
