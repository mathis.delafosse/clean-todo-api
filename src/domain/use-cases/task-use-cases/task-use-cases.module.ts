import { Module } from '@nestjs/common';
import { CreateTaskUseCase } from './create-task.use-case';
import { GetTasksUseCase } from './get-tasks.use-case';
import { GetTaskByIdUseCase } from './get-task-by-id.use-case';
import { UpdateTaskUseCase } from './update-task.use-case';
import { DeleteTaskUseCase } from './delete-task.use-case';
import { InfraModule } from '../../../infra/infra.module';
import { GetTasksByStatusUseCase } from './get-tasks-by-status-use.case';

@Module({
  imports: [InfraModule],
  providers: [
    CreateTaskUseCase,
    GetTasksUseCase,
    GetTaskByIdUseCase,
    GetTasksByStatusUseCase,
    UpdateTaskUseCase,
    DeleteTaskUseCase,
  ],
  exports: [
    CreateTaskUseCase,
    GetTasksUseCase,
    GetTaskByIdUseCase,
    GetTasksByStatusUseCase,
    UpdateTaskUseCase,
    DeleteTaskUseCase,
  ],
})
export class TaskUseCasesModule {}
