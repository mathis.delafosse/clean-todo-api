import { ITaskRepository } from '../../adapters/task-repository';
import { Task } from '../../models/task.entity';
import { Inject } from '@nestjs/common';
import { TASK_REPOSITORY_TOKEN } from '../../../presenters/inject-tokens';

export class UpdateTaskUseCase {
  constructor(
    @Inject(TASK_REPOSITORY_TOKEN) private taskRepository: ITaskRepository,
  ) {}

  execute(task: Task) {
    return this.taskRepository.updateTask(task);
  }
}
