import { ITaskRepository } from '../../adapters/task-repository';
import { Inject } from '@nestjs/common';
import { TASK_REPOSITORY_TOKEN } from '../../../presenters/inject-tokens';

export class GetTasksUseCase {
  constructor(
    @Inject(TASK_REPOSITORY_TOKEN) private taskRepository: ITaskRepository,
  ) {}

  execute() {
    return this.taskRepository.getTasks();
  }
}
