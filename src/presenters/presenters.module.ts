import { Module } from '@nestjs/common';
import { TaskUseCasesModule } from '../domain/use-cases/task-use-cases/task-use-cases.module';
import { TaskController } from './task/task.controller';

@Module({
  imports: [TaskUseCasesModule],
  controllers: [TaskController],
})
export class PresentersModule {}
