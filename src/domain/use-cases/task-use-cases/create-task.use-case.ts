import { ITaskRepository } from '../../adapters/task-repository';
import { Inject, Injectable } from '@nestjs/common';
import { TASK_REPOSITORY_TOKEN } from '../../../presenters/inject-tokens';

@Injectable()
export class CreateTaskUseCase {
  constructor(
    @Inject(TASK_REPOSITORY_TOKEN) private taskRepository: ITaskRepository,
  ) {}

  execute(title: string, description: string) {
    return this.taskRepository.createTask(title, description);
  }
}
