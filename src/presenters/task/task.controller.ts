import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateTaskUseCase } from '../../domain/use-cases/task-use-cases/create-task.use-case';
import { GetTasksUseCase } from '../../domain/use-cases/task-use-cases/get-tasks.use-case';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateTaskDto } from '../../domain/dtos/create-task.dto';
import { Task } from '../../domain/models/task.entity';
import { GetTaskByIdUseCase } from '../../domain/use-cases/task-use-cases/get-task-by-id.use-case';
import { UpdateTaskUseCase } from '../../domain/use-cases/task-use-cases/update-task.use-case';
import { DeleteTaskUseCase } from '../../domain/use-cases/task-use-cases/delete-task.use-case';
import { GetTasksByStatusUseCase } from '../../domain/use-cases/task-use-cases/get-tasks-by-status-use.case';
import { TaskStatus } from '../../domain/models/task-status.model';

@ApiTags('tasks')
@Controller('tasks')
export class TaskController {
  constructor(
    private createTaskUseCase: CreateTaskUseCase,
    private getTasksUseCase: GetTasksUseCase,
    private getTaskByIdUseCase: GetTaskByIdUseCase,
    private getTaskByStatusUseCase: GetTasksByStatusUseCase,
    private updateTaskUseCase: UpdateTaskUseCase,
    private deleteTaskUseCase: DeleteTaskUseCase,
  ) {}

  @Post('/:taskId')
  @ApiOperation({ summary: 'Create a task' })
  @ApiCreatedResponse({
    description: 'The task has been successfully created.',
    type: Task,
  })
  createTask(@Body() createTaskDto: CreateTaskDto) {
    return this.createTaskUseCase.execute(
      createTaskDto.title,
      createTaskDto.description,
    );
  }

  @Get()
  @ApiOperation({ summary: 'Get all tasks' })
  @ApiResponse({
    type: Task,
  })
  getTasks() {
    return this.getTasksUseCase.execute();
  }

  @Get('/:taskId')
  @ApiOperation({ summary: 'Get a task by id' })
  @ApiResponse({
    type: Task,
  })
  getTaskById(@Param('taskId') taskId: string) {
    return this.getTaskByIdUseCase.execute(taskId);
  }

  @Get('/status/:status')
  @ApiOperation({ summary: 'Get tasks with a specific status' })
  @ApiParam({
    name: 'status',
    enum: TaskStatus,
  })
  @ApiResponse({
    type: Task,
  })
  getTasksByStatus(@Param('status') status: TaskStatus) {
    return this.getTaskByStatusUseCase.execute(status);
  }

  @Put()
  @ApiOperation({ summary: 'Update a task' })
  @ApiResponse({
    description: 'The task has been successfully updated.',
    type: Task,
  })
  updateTask(@Body() task: Task) {
    return this.updateTaskUseCase.execute(task);
  }

  @Delete('/:taskId')
  @ApiOperation({ summary: 'Delete a task' })
  @ApiResponse({
    description: 'The task has been successfully deleted.',
    type: Task,
  })
  deleteTask(@Param('taskId') taskId: string) {
    return this.deleteTaskUseCase.execute(taskId);
  }
}
