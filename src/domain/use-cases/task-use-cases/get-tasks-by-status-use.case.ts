import { ITaskRepository } from '../../adapters/task-repository';
import { Inject } from '@nestjs/common';
import { TASK_REPOSITORY_TOKEN } from '../../../presenters/inject-tokens';
import { TaskStatus } from '../../models/task-status.model';

export class GetTasksByStatusUseCase {
  constructor(
    @Inject(TASK_REPOSITORY_TOKEN) private taskRepository: ITaskRepository,
  ) {}

  execute(status: TaskStatus) {
    return this.taskRepository.getTasksByStatus(status);
  }
}
