import { ITaskRepository } from '../domain/adapters/task-repository';
import { Observable, of } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { Task } from '../domain/models/task.entity';
import { TaskStatus } from '../domain/models/task-status.model';
import * as fs from 'fs';

@Injectable()
export class JsonTaskRepository implements ITaskRepository {
  private tasks = new Map<string, any>();
  private fileName = 'tasks.json';

  _getTasksFromFile(): Task[] {
    try {
      return JSON.parse(fs.readFileSync('tasks.json', 'utf8'));
    } catch (e) {
      return [];
    }
  }

  _saveTasksToFile(tasks: Task[]) {
    fs.writeFileSync('tasks.json', JSON.stringify(tasks));
  }

  createTask(title: string, description: string): Observable<Task> {
    const tasks = this._getTasksFromFile();
    const task = new Task(
      tasks.length.toString(),
      title,
      description,
      TaskStatus.OPEN,
    );
    tasks.push(task);
    this._saveTasksToFile(tasks);

    return of(task);
  }

  getTasks() {
    return of(this._getTasksFromFile());
  }

  getTaskById(id: string): Observable<Task | null> {
    return of(this._getTasksFromFile().find((task) => task.id === id));
  }

  getTasksByStatus(status: TaskStatus): Observable<Task[]> {
    return of(
      this._getTasksFromFile().filter((task) => task.status === status),
    );
  }

  updateTask(task: Task): Observable<Task> {
    let tasks = this._getTasksFromFile();
    tasks = tasks.map((t) => (t.id === task.id ? task : t));
    this._saveTasksToFile(tasks);

    return of(task);
  }

  deleteTask(id: string): Observable<Task> {
    let tasks = this._getTasksFromFile();
    const task = tasks.find((task) => task.id === id);
    tasks = tasks.filter((task) => task.id !== id);
    this._saveTasksToFile(tasks);

    return of(task);
  }
}
