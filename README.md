## Description
Clean Architecture Todo API with NestJS.
This is a simple Todo API that uses NestJS and Clean Architecture.
Two data providers are available: JSON and InMemory to demonstrate the flexibility of the architecture.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# start with json data provider
$ npm run start-json

# start with in memory data provider
$ npm run start-inmemory

# production mode
$ npm run start:prod
```

## Swagger
Test the API with Swagger :
http://localhost:3000/swagger
